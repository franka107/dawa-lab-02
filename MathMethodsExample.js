
let absolute = Math.abs(-12)
console.log(absolute)
//Rpta: 12

let acos =  Math.acos(0)
console.log(acos)

let ceil = Math.ceil(3.1)
console.log(ceil)

let potencia = Math.pow(5,2)
console.log(potencia)

let random = Math.random()
console.log(random)

console.log(`
  ${Math.E},
  ${Math.PI},
  ${Math.SQRT2},
  ${Math.SQRT1_2},
  ${Math.LN2},
  ${Math.LN10}, 
  ${Math.LOG2E},
  ${Math.LOG10E},
`)

let palabra = "Escarabajo"
console.log(palabra.charAt(5))
console.log(palabra.charCodeAt(5))
console.log(palabra.endsWith('o'))
console.log( palabra.split('').reverse().join('-'))